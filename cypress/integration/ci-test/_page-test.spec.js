context("page-test", () => {
    beforeEach(() => {
        cy.visit("https://xenophon566.gitlab.io/cypressplayground/#/page-test");
    });

    it("表格是否存在", () => {
        cy.get("tbody > :nth-child(1) > th").should("be.visible");
    });

    it("確認鈕是否存在", () => {
        cy.get(".btn-primary").should("be.visible");
    });

    it("點擊確認鈕後出現 Confirm!!!字串", () => {
        cy.get(".btn-primary")
            .click()
            .then(() => {
                cy.get(".msg").should("be.visible");
                cy.wait(2e3);
                cy.get(".btn-dark").click();
            });
    });
});
