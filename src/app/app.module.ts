import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RwdTestComponent } from './rwd-test/rwd-test.component';
import { SafePipe } from './safe.pipe';
import { PageTestComponent } from './page-test/page-test.component';

@NgModule({
    declarations: [AppComponent, HomeComponent, RwdTestComponent, SafePipe, PageTestComponent],
    imports: [BrowserModule, AppRoutingModule],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
    bootstrap: [AppComponent],
})
export class AppModule {}
