import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';

describe('## HomeComponent 頁面測試', () => {
    beforeEach(() => {
        const spy = jasmine.createSpyObj('UtilitiesService', [
            'navigateTo',
            'getMockSession',
            'configParser',
            'getUrlQueryString',
        ]);

        TestBed.configureTestingModule({
            declarations: [HomeComponent],
            imports: [HttpClientTestingModule, RouterTestingModule, RouterTestingModule.withRoutes([])],
        }).compileComponents();
    });

    describe('### 自動測試初始化', () => {
        it('### HomeComponent存在', () => {
            expect(HomeComponent).toBeTruthy();
        });
    });
});
