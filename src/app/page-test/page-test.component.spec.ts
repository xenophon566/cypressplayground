import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PageTestComponent } from './page-test.component';

describe('## PageTestComponent 頁面測試', () => {
    beforeEach(() => {
        const spy = jasmine.createSpyObj('UtilitiesService', [
            'navigateTo',
            'getMockSession',
            'configParser',
            'getUrlQueryString',
        ]);

        TestBed.configureTestingModule({
            declarations: [PageTestComponent],
            imports: [HttpClientTestingModule, RouterTestingModule, RouterTestingModule.withRoutes([])],
        }).compileComponents();
    });

    describe('### 自動測試初始化', () => {
        it('### PageTestComponent存在', () => {
            expect(PageTestComponent).toBeTruthy();
        });
    });
});
