import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-test',
    templateUrl: './page-test.component.html',
    styleUrls: ['./page-test.component.scss'],
})
export class PageTestComponent implements OnInit {
    constructor() {}

    msgText = '';

    confirm() {
        this.msgText = 'Confirm!!!';
    }

    cancel() {
        this.msgText = '';
    }

    ngOnInit(): void {}
}
