import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RwdTestComponent } from './rwd-test.component';

describe('## RwdTestComponent 頁面測試', () => {
    beforeEach(() => {
        const spy = jasmine.createSpyObj('UtilitiesService', [
            'navigateTo',
            'getMockSession',
            'configParser',
            'getUrlQueryString',
        ]);

        TestBed.configureTestingModule({
            declarations: [RwdTestComponent],
            imports: [HttpClientTestingModule, RouterTestingModule, RouterTestingModule.withRoutes([])],
        }).compileComponents();
    });

    describe('### 自動測試初始化', () => {
        it('### RwdTestComponent存在', () => {
            expect(RwdTestComponent).toBeTruthy();
        });
    });
});
